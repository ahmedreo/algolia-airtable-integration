const algolia = require('@/services/algolia')
const convertapi = require('@/services/convertapi')
const request = require('request');
var fs = require('fs');
var https = require('https');

var download = function(url, dest , username) {
  var file = fs.createWriteStream(dest);
  var request = https.get(encodeURI(url), function(response) {
    response.pipe(file);
    file.on('finish', function() {
      file.end();
			convertapi.convert('pdf', { File: dest })
			.then(function(result) {
				cloudinary.uploader.upload(result.file.url , {public_id: 'ProfileResumes/' + username} , (err , result) => {
					console.log(err )
				})
			})
    });
  })
};

module.exports = (req , res) => {
	algolia.browse({query: ''} , (err, { hits } = {}) => {
		if (err) throw err;
		hits.forEach(i => {
			var extension = i.tfresumelink.split('.').pop()
			if(extension !== 'pdf'){
					download(i.tfresumelink , 'storage/' + i.username + '.' + extension , i.username)
			}
			else {
				cloudinary.uploader.upload(i.tfresumelink , {public_id: 'ProfileResumes/' + i.username} , (err , result) => {
					console.log(err )
				})
			}
		})
		res.send('done')
	});
}
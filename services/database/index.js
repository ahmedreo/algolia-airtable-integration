const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/chato-new' , { 
	useNewUrlParser: true,
	useCreateIndex: true, 
});

module.exports = mongoose
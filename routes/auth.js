const Auth = require('express').Router()
const RegisterMiddleware = require('@/middlewares/RegisterMiddleware')

Auth.get('/register' , [RegisterMiddleware] , require('@/controllers/auth/register'))
Auth.post('/login' , require('@/controllers/auth/login'))
Auth.get('/logout' , require('@/controllers/auth/logout'))

module.exports = Auth 
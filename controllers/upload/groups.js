const algolia = require('@/services/algolia')
const airtable = require('@/services/airtable')
const index = algolia.initIndex('groups_2');

module.exports = (req , res) => {
	const groupsTable = airtable.table('Student Groups')
	var hits = [] 
	groupsTable.list({maxRecords: 400}).then(response => {
		if (response.offset === undefined) {
			response.records.forEach(group => {
				if (group.fields.logo) {
					group.fields.logo = group.fields.logo[0]['url']
				}
				if (!group.fields.members) {
					group.fields.members = []
				}
				group.fields.id = group.id
				hits.push(group.fields)
			})
			index.addObjects(hits , (err , content) => {
				res.send(err)
			})	
		}
	})
}
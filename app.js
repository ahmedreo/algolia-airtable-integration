const express = require('express')
const app = express()
const bodyParser = require('body-parser');

require('module-alias/register')
app.use(bodyParser.json());
require('./routes')(app)

const port = 3000

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

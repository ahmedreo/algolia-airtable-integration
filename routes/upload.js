const Upload = require('express').Router()

Upload.get('/images' , require('@/controllers/upload/images.js'))
Upload.get('/files' , require('@/controllers/upload/files.js'))
Upload.get('/members' , require('@/controllers/upload/members.js'))
Upload.get('/groups' , require('@/controllers/upload/groups.js'))

module.exports = Upload


const algolia = require('@/services/algolia')
const airtable = require('@/services/airtable')
const index = algolia.initIndex('members_3');


module.exports = (req, res) => {
	const membersTable = airtable.table('Members')

	let hits = []

	membersTable.list({ maxRecords: 800 }).then(resp => {
		if (resp.offset === undefined) {
			resp.records.forEach(i => {
				if (i.fields.resume) {
					i.fields.resume = i.fields.resume[0]['url']
				}
				if (i.fields.picture) {
					i.fields.picture = i.fields.picture[0]['url']
				}
				if (!i.fields.visible) {
					i.fields.visible = false
				}
				else {
					i.fields.visible = true
				}
				if (i.fields.gender) {
					i.fields.gender.forEach(gen => {
						i.fields[gen.toLowerCase()] = gen
					})
				}
				if (i.fields.ethnicity) {
					i.fields.ethnicity.forEach(ethn => {
						i.fields[ethn.toLowerCase()] = ethn
					})
				}
				if (i.fields.experiences) {
					i.fields.experiences.forEach(exp => {
						i.fields[exp.toLowerCase()] = exp
					})
				}
				if (!i.fields.groups) {
					i.fields.groups = []
				}
				if (i.fields.gpa) {
					i.fields.gpa_format = i.fields.gpa * 100
				}
				i.fields.id = i.id
				hits.push(i.fields)
			})
			index.addObjects(hits, (err, content) => {
				res.send(err)
			})
		}
	})
}
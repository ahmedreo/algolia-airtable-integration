const Api = require('express').Router()

Api.use('/auth' , require('./auth.js'))
Api.use('/upload' , require('./upload.js'))

module.exports = Api